This repository contains an implementation of the CMake tutorial that can be found at https://cmake.org/cmake-tutorial/

Please read and follow this tutorial and look at the solution when needed.
1. Implement step 1 and 2 of the CMake tutorial
2. Apply what you have learned on the carrot_juice project and replace the compile.sh script with a proper CMake 
3. Implement step 3 and 4 of the CMake tutorial
4. Apply what you have learned on the carrot_juice project and add a install target as well as ctest